# README #

Ini jawaban jurnal modul 4 tentang JSON dan jQuery.

Untuk metode $.getjson() terkadang dibeberapa kasus tidak perlu menggunakan server, jadi ada kemungkinan source code ini dapat berjalan tanpa harus menggunakan XAMPP. Akan tetapi jika source code tidak dapat dijalankan tanpa server, maka seperti yang dijelaskan pada soal jurnal XAMPP harus diaktifkan dan pindahkan source code-nya ke folder 'htdocs/nama-folder', dan diakses menggunakan localhost/nama-project.